'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');

/*TASK FOR APP START*/

gulp.task('sass', function () {
    return gulp.src('./src/scss/**/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./app/css'));
});

gulp.task('copy-html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./app'));
});

gulp.task('copy-js', function () {
    gulp.src('./src/**/*.js')
        .pipe(gulp.dest('./app'));
});

gulp.task('copy-img', function () {
    gulp.src('./src/img/**/*.*')
        .pipe(gulp.dest('./app/img'));
});

/*TASK FOR APP END*/

/*TASK FOR DIST START*/

gulp.task('copy-html-dist', ['clean'], function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('mini-img', ['copy-html-dist'], function () {
    gulp.src('./src/img/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'));
});

gulp.task("mini-js", ['mini-css'], function () {
    return gulp.src('./src/js/script.js')
        .pipe(minify())
        .pipe(concat('script.min.js'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task("mini-css", ['mini-img'], function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(cleanCSS())
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('clean', function () {
    return gulp.src('./dist/*', {read: false})
        .pipe(clean());
});

gulp.task('bild:dist', ['clean', 'copy-html-dist', 'mini-img', 'mini-css', 'mini-js'], function () {
    gulp.dest('./dist/');
});

/*TASK FOR DIST END*/



gulp.task('serve', ['copy-img'], function () {
   browserSync.init({
       server: './app'
   });

   gulp.watch('./src/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
   gulp.watch('./src/**/*.html', ['copy-html']).on('change', browserSync.reload);
   gulp.watch('./src/img/**/*.*', ['copy-js']).on('change', browserSync.reload);
});

gulp.task('default', ['serve']);


